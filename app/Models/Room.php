<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;
    protected $fillable = [
        'room_name',
        'room_qty',
        'address',
        'is_published'
    ];

    public function hotel() {
        return $this->belongsTo(Hotel::class);
    }

}
