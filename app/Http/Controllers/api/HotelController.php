<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Hotel;
use App\Models\Room;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use function GuzzleHttp\Promise\all;

class HotelController extends Controller
{

    public function availableRooms($hotel)
    {
        $currentHotel = $this->getHotel($hotel);
        return $currentHotel->room->where("is_occupied", 0);
    }

    public function allRoomsInfo($hotel)
    {
        $currentHotel = $this->getHotel($hotel);

        $availableRooms = $currentHotel->room->where("is_occupied", 0);
        $occupiedRooms = $currentHotel->room->where("is_occupied", 1);
        return \response(["Available Rooms Amount" => count($availableRooms), "Occupied Rooms Amount" => count($occupiedRooms)]);
    }

    public function reserveRoom($hotel, $roomName, Request $request)
    {
        $currentHotel = $this->getHotel($hotel);
        $room = $currentHotel->room->where("room_name", $roomName)->first();
        if ($room->is_occupied == 1) {
            return \response(["Warning" => "The room is occupied"]);
        }
        $email = $request->get("email");
        $currentRoom = Room::all()->where("room_name", $roomName)->first();
        $this->reserveHotelRoom($currentRoom, $email);
        return \response(["message" => "Successfully reserved!", "Room" => $currentRoom]);
    }

    private function getHotel($hotelName)
    {
        return Hotel::all()->first(function ($item) use ($hotelName) {
            return $item->name == $hotelName;
        });
    }

    private function reserveHotelRoom($room, $owner) {
        $room->occupied_by = $owner;
        $room->is_occupied = 1;
        $room->save();
    }
}
