<?php

use App\Http\Controllers\api\AuthController;
use App\Http\Controllers\api\HotelController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::middleware("auth:api")->group(function () {
    Route::post("hotels/{hotel}/rooms/{room}/reserve", [HotelController::class, "reserveRoom"]);
});

Route::get("hotels/{hotel}/available-rooms", [HotelController::class, "availableRooms"]);
Route::get("hotels/{hotel}/", [HotelController::class, "allRoomsInfo"]);

Route::post("register", [AuthController::class, "register"]);
Route::post("login", [AuthController::class, "login"]);
